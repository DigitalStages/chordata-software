thickness=6;
width=50;
bHeight=15;
diameter=10;
$fn=30;
height=50;

btm=true;
mid=true;
top=true;
lid=true;
cvr=false;

//=============================================================

//Bottom Part
if(btm)
{
    //Band holder
    translate([52,0,-6])
    rotate([90,0,0])
    cylinder(h=height+diameter*2,d=7,center=true);
    translate([-52,0,-6])
    rotate([90,0,0])
    cylinder(h=height+diameter*2,d=7,center=true);
    difference()
    {
        union()
        {
            //Base btm
            hull()
            {
                translate([-32,-31,-5])
                cylinder(h=thickness,d=diameter+2);
                translate([32,-31,-5])
                cylinder(h=thickness,d=diameter+2);
                translate([32,31,-5])
                cylinder(h=thickness,d=diameter+2);
                translate([-32,31,-5])
                cylinder(h=thickness,d=diameter+2);
            }
            //Band holder frame
            difference()
            {
                scale([1,1,0.2])
                translate([0,0,-30])
                rotate([90,0,0])
                cylinder(h=width*1.4,d=110,$fn=80,center=true);
                scale([1,1,0.3])
                translate([0,0,-100])
                rotate([90,0,0])
                cylinder(h=width*1.4,d=190,$fn=80,center=true);
                translate([-46,0,-7])
                rotate([0,-30,0])
                cube([7,50,20],center=true);
                translate([46,0,-7])
                rotate([0,30,0])
                cube([7,50,20],center=true);
            }
            //Base top
            difference()
            {
                translate([0,0,thickness/2])   
                hull()
                {
                    translate([-32,30,1])
                    cylinder(h=thickness-2,d=18,center=true);
                    translate([32,30,1])
                    cylinder(h=thickness-2,d=18,center=true);
                    translate([-32,-30,1])
                    cylinder(h=thickness-2,d=18,center=true);
                    translate([32,-30,1])
                    cylinder(h=thickness-2,d=18,center=true);
                }
            }
        }
        //Patch space
        translate([0,0,-6])
        cube([70,60,10],center=true);
        //Holes for Mid Part
        translate([-30,30,1])
        cylinder(h=6,d=3);
        translate([30,30,1])
        cylinder(h=6,d=3);
        translate([30,-30,1])
        cylinder(h=6,d=3);
        translate([-30,-30,1])
        cylinder(h=6,d=3);
    }
}

//============================================================

//Middle Part
if(mid)
{
    union()
    {
       rotate([0,180,90])
       translate([0,-1,-57])
       difference()
        {
            union()
            {
                difference()
                {
                    //Frame for top frame part
                    translate([0,0,thickness-2+22])
                    hull()
                    {
                        translate([-39,32,0])
                        cylinder(h=8,d=22);
                        translate([41,32,0])
                        cylinder(h=8,d=22);
                        translate([-39,-32,0])
                        cylinder(h=8,d=22);
                        translate([41,-32,0])
                        cylinder(h=8,d=22);
                    }
                    //Cutout of frame part
                    translate([0,0,thickness-2+22])
                    hull()
                    {
                        translate([-38,31,0])
                        cylinder(h=8,d=18);
                        translate([40,31,0])
                        cylinder(h=8,d=18);
                        translate([-38,-31,0])
                        cylinder(h=8,d=18);
                        translate([40,-31,0])
                        cylinder(h=8,d=18);
                    }  
                }
                difference()
                {
                    //Plug pipes
                    union()
                    {
                        translate([0,8,6])
                        union()
                        {
                            //Mid R
                            hull()
                            {
                                translate([0,0,36])
                                rotate([0,90,0])
                                cylinder(h=5,d=24,center=true);
                                translate([50,0,36])
                                rotate([0,90,0])
                                cylinder(h=5,d=32,center=true);
                                translate([56,0,36])
                                rotate([0,90,0])
                                cylinder(h=2,d=25,center=true);
                            }
                            //Btm R
                            hull()
                            {
                                translate([0,-18,36])
                                rotate([0,90,0])
                                cylinder(h=5,d=24,center=true);
                                translate([49.5,-22,36])
                                rotate([0,90,-5])
                                cylinder(h=5,d=32,center=true);
                                translate([54.9,-22,36])
                                rotate([0,90,-5])
                                cylinder(h=2,d=25,center=true);
                            }
                            //Top R
                            hull()
                            {
                                translate([0,18,36])
                                rotate([0,90,0])
                                cylinder(h=5,d=24,center=true);
                                translate([49.5,22,36])
                                rotate([0,90,5])
                                cylinder(h=5,d=32,center=true);
                                translate([54.9,22,36])
                                rotate([0,90,5])
                                cylinder(h=2,d=25,center=true);
                            }
                        }
                        //
                        translate([0,8,6])
                        mirror([1,0,0])
                        union()
                        {
                            //Mid L
                            hull()
                            {
                                translate([0,0,36])
                                rotate([0,90,0])
                                cylinder(h=5,d=24,center=true);
                                translate([49,0,36])
                                rotate([0,90,0])
                                cylinder(h=5,d=32,center=true);
                                translate([55,0,36])
                                rotate([0,90,0])
                                cylinder(h=2,d=25,center=true);
                            }
                            //Btm L
                            hull()
                            {
                                translate([0,-18,36])
                                rotate([0,90,0])
                                cylinder(h=5,d=24,center=true);
                                translate([48.5,-22,36])
                                rotate([0,90,-5])
                                cylinder(h=5,d=32,center=true);
                                translate([53.9,-22,36])
                                rotate([0,90,-5])
                                cylinder(h=2,d=25,center=true);
                            }
                            //Top L
                            hull()
                            {
                                translate([0,18,36])
                                rotate([0,90,0])
                                cylinder(h=5,d=24,center=true);
                                translate([48.5,22,36])
                                rotate([0,90,5])
                                cylinder(h=5,d=32,center=true);
                                translate([53.9,22,36])
                                rotate([0,90,5])
                                cylinder(h=2,d=25,center=true);
                            }
                        }
                        //Body part
                        translate([0,12,35])
                        union()
                        {
                            //Side
                            hull()
                            {
                                translate([.8,24,8])
                                scale([1.3,0.6,1])
                                cylinder(h=19,d=80,center=true,$fn=80);
                                translate([40,-43,8])
                                cylinder(h=19,d=20,center=true);
                                translate([-39,-43,8])
                                cylinder(h=19,d=20,center=true);
                            }
                        }
                    }
                    union()
                    {
                        //Pipe cutoff
                        translate([0,0,24])
                        cube([100,79,20],center=true);
                        //Socket cutout for bottom part
                        translate([0,0,60])   
                        hull()
                        {
                            translate([-31,34,-1.5])
                            cylinder(h=12,d=18,center=true);
                            translate([31,34,-1.5])
                            cylinder(h=12,d=18,center=true);
                            translate([-31,-31,-1.5])
                            cylinder(h=12,d=18,center=true);
                            translate([31,-31,-1.5])
                            cylinder(h=12,d=18,center=true);
                        }
                        //Holes to bottom
                        translate([-30,31,47])
                        cylinder(h=9,d=3);
                        translate([30,31,47])
                        cylinder(h=9,d=3);
                        translate([-30,-29,47])
                        cylinder(h=9,d=3);
                        translate([30,-29,47])
                        cylinder(h=9,d=3);
                        translate([-30,31,49])
                        cylinder(h=3,d=7);
                        translate([30,31,49])
                        cylinder(h=3,d=7);
                        translate([-30,-29,49])
                        cylinder(h=3,d=7);
                        translate([30,-29,49])
                        cylinder(h=3,d=7);
                    }
                }
            }
            //Cutouts
            union()
            {
                //Inroom cutout
                translate([0,0,29])
                hull()
                {
                    translate([-35.5,27.5,0])
                    cylinder(h=20,d=18);
                    translate([37.5,27.5,0])
                    cylinder(h=20,d=18);
                    translate([-35.5,-27.5,0])
                    cylinder(h=20,d=18);
                    translate([37.5,-27.5,0])
                    cylinder(h=20,d=18);
                }
                //Ethernet holes R
                translate([43,8,41])
                cube([40,15,13],center=true);
                rotate([0,0,-5])
                translate([43,-9.5,41])
                cube([40,15,13],center=true);
                rotate([0,0,5])
                translate([43,25,41])
                cube([40,15,13],center=true);
                //Ethernet holes L
                translate([-42,8,41])
                cube([40,15,13],center=true);
                rotate([0,0,5])
                translate([-42,-9.5,41])
                cube([40,15,13],center=true);
                rotate([0,0,-5])
                translate([-42,25,41])
                cube([40,15,13],center=true);
                translate([.8,36,42])
                scale([1.3,0.6,1])
                cylinder(h=14,d=77,center=true,$fn=80);
                //Switch Hole
                translate([-12,53,43])
                rotate([0,0,7])
                cube([11,15,5],center=true);
                //LED Hole
                translate([-27,52,43])
                rotate([90,0,20])
                cylinder(h=9,d=3.5,center=true);
                //Button hole
                translate([20,54,43])
                rotate([90,0,-5])
                cylinder(h=9,d=4.5,center=true);
                //Cable hole
                translate([48,-33,40])
                rotate([0,90,0])
                cylinder(h=9,d=5,center=true);
                //Cut 2 corners
                translate([-29,34,34])
                cube([30,10,4],center=true);
                translate([31,34,34])
                cube([30,10,4],center=true);
                //Screw holes for Cable Clamps
                translate([53,8,51])
                rotate([0,90,0])
                cylinder(h=6,d=3);
                translate([53,30,51])
                rotate([0,90,0])
                cylinder(h=6,d=3);
                translate([53,-14.3,51])
                rotate([0,90,0])
                cylinder(h=6,d=3);
                translate([-58,8,51])
                rotate([0,90,0])
                cylinder(h=6,d=3);
                translate([-58,30,51])
                rotate([0,90,0])
                cylinder(h=6,d=3);
                translate([-58,-14.3,51])
                rotate([0,90,0])
                cylinder(h=6,d=3);
                //Screw holes for top part connection
                translate([27,-38,30])
                rotate([90,0,0])
                cylinder(h=8,d=3);
                translate([27,46,30])
                rotate([90,0,0])
                cylinder(h=8,d=3);
                translate([-23,-38,30])
                rotate([90,0,0])
                cylinder(h=8,d=3);
                translate([-23,46,30])
                rotate([90,0,0])
                cylinder(h=8,d=3);
                //Sink holes for screws top part connection
                translate([27,-41,30])
                rotate([90,0,0])
                cylinder(h=3,d=7);
                translate([27,44,30])
                rotate([90,0,0])
                cylinder(h=3,d=7);
                translate([-23,-41,30])
                rotate([90,0,0])
                cylinder(h=3,d=7);
                translate([-23,44,30])
                rotate([90,0,0])
                cylinder(h=3,d=7);
            }
        }
    }
}

//============================================================

//Top Part
if(top)
{
    union()
    {
        translate([1,-2,62])
        rotate([180,0,90])
        union()
        {
            difference()
            {
                //SideWalls
                difference()
                {
                    translate([0,0,thickness-2])
                    union()
                    {
                        
                        hull()
                        {
                            //Upper part walls
                            translate([-36,30.5,4])
                            cylinder(h=3,d=18);
                            translate([37,30.5,4])
                            cylinder(h=3,d=18);
                            translate([-36,-30.5,4])
                            cylinder(h=3,d=18);
                            translate([37,-30.5,4])
                            cylinder(h=3,d=18);
                            //Lower part walls
                            translate([-38,30.5,32])
                            cylinder(h=3,d=18);
                            translate([39,30.5,32])
                            cylinder(h=3,d=18);
                            translate([-38,-30.5,32])
                            cylinder(h=3,d=18);
                            translate([39,-30.5,32])
                            cylinder(h=3,d=18);                    
                        }
                        //UpperPart: Frame
                        hull()
                        {
                            translate([-36,30.5,0])
                            cylinder(h=4,d=18);
                            translate([37,30.5,0])
                            cylinder(h=4,d=18);
                            translate([-36,-30.5,0])
                            cylinder(h=4,d=18);
                            translate([37,-30.5,0])
                            cylinder(h=4,d=18);
                        }
                    }
                    //SideWalls Cutouts
                    translate([0,0,thickness-2])
                    hull()
                    {
                        //Cutout upper part
                        translate([-34.5,29,4])
                        cylinder(h=3,d=15);
                        translate([35.5,29,4])
                        cylinder(h=3,d=15);
                        translate([-34.5,-29,4])
                        cylinder(h=3,d=15);
                        translate([35.5,-29,4])
                        cylinder(h=3,d=15);
                        //Cutout lower part
                        translate([-37,29,32])
                        cylinder(h=3,d=15);
                        translate([38,29,32])
                        cylinder(h=3,d=15);
                        translate([-37,-29,32])
                        cylinder(h=3,d=15);
                        translate([38,-29,32])
                        cylinder(h=3,d=15);
                    }
                }
                //Cutout parts
                union()
                {
                    //Ethernet/USB holes
                    translate([-50,17,13.5])
                    cube([10,16,16]);
                    translate([-50,-1,13.5])
                    cube([10,16,16]);
                    translate([-50,-20,15.5])
                    cube([10,17,14]);
                    //Connetor holes to mid part of case
                    translate([25,-36,35])
                    rotate([90,0,0])
                    cylinder(h=6,d=3);
                    translate([25,41,35])
                    rotate([90,0,0])
                    cylinder(h=6,d=3);
                    translate([-25,-36,35])
                    rotate([90,0,0])
                    cylinder(h=6,d=3);
                    translate([-25,41,35])
                    rotate([90,0,0])
                    cylinder(h=6,d=3);
                    //SD card slot
                    translate([43,-1,bHeight+thickness+8])
                    cube([5,13,3]);
                    //Upper frame cutout
                    translate([0,0,thickness-2])
                    hull()
                    {
                        translate([-36,30,0])
                        cylinder(h=4,d=15);
                        translate([37,30,0])
                        cylinder(h=4,d=15);
                        translate([-36,-30,0])
                        cylinder(h=4,d=15);
                        translate([37,-30,0])
                        cylinder(h=4,d=15);
                    }
                }
            }
            //Rpi socket
            difference()
            {
                union()
                {
                    //Pillars
                    translate([-19,30,thickness+2])
                    cylinder(h=bHeight+5,d=6);
                    translate([39,30,thickness+2])
                    cylinder(h=bHeight+5,d=6);
                    translate([-19,-19,thickness+2])
                    cylinder(h=bHeight+5,d=6);
                    translate([39,-19,thickness+2])
                    cylinder(h=bHeight+5,d=6);
                    //Board positioner
                    translate([26,0,thickness+2])
                    hull()
                    {

                        translate([12,30,1])
                        cylinder(h=2,d=15,center=true);
                        translate([12,-30,1])
                        cylinder(h=2,d=15,center=true);
                        translate([-42,0,1])
                        cube([15,74,2],center=true);
                    }
                }
                //Cutouts
                union()
                {
                    //Pillar screw holes
                    translate([-19,30,thickness+17])
                    cylinder(h=6,d=3);
                    translate([39,30,thickness+17])
                    cylinder(h=6,d=3);
                    translate([-19,-19,thickness+17])
                    cylinder(h=6,d=3);
                    translate([39,-19,thickness+17])
                    cylinder(h=6,d=3);
                    //Lid leveler
                    translate([-19,21,3])
                    cylinder(h=6,d=3);
                    translate([30,30,5])
                    cylinder(h=6,d=3);
                    translate([-19,-29,3])
                    cylinder(h=6,d=3);
                    translate([37,-29,5])
                    cylinder(h=6,d=3);
                    translate([0,2,11])
                    cube([60,30,10],center=true);
                    translate([11,0,10])
                    cube([40,46,10],center=true);
                }
            }
        }
    }
}

//============================================================

//Lid Part
if(lid)
{
    translate([1,-2,55])
    rotate([0,0,90])
    difference()
    {
        union()
        {
            hull()
            {
                hull()
                {
                    translate([-34.8,28.8,5])
                    cylinder(h=2,d=16);
                    translate([34.8,28.8,5])
                    cylinder(h=2,d=16);
                    translate([-34.8,-28.8,5])
                    cylinder(h=2,d=16);
                    translate([34.8,-28.8,5])
                    cylinder(h=2,d=16);
                }
                hull()
                {
                    translate([-37,31.5,3])
                    cylinder(h=2,d=16);
                    translate([38,31.5,3])
                    cylinder(h=2,d=16);
                    translate([-37,-31.5,3])
                    cylinder(h=2,d=16);
                    translate([38,-31.5,3])
                    cylinder(h=2,d=16);
                }
            }
            hull()
            {
                translate([-34.8,28.8,-1])
                cylinder(h=6,d=16);
                translate([34.8,28.8,-1])
                cylinder(h=6,d=16);
                translate([-34.8,-28.8,-1])
                cylinder(h=6,d=16);
                translate([34.8,-28.8,-1])
                cylinder(h=6,d=16);
            }
        }
        union()
        {
            translate([-19,29,-3])
            cylinder(h=6,d=3);
            translate([37,29,-3])
            cylinder(h=6,d=3);
            translate([-19,-21,-3])
            cylinder(h=6,d=3);
            translate([30,-30,-3])
            cylinder(h=6,d=3);
        }
    }
}

//============================================================

//Cover for Ethernet wires

if(cvr)
{     
    difference()
    {
        union()
        {
            translate([0,0,37])
            cube([5,14.5,12.5],center=true);
            difference()
            {
                translate([4,0,36])
                rotate([0,90,0])
                cube([18,18,3],center=true);
                difference()
                {
                    translate([4,0,36])
                    rotate([0,90,0])
                    cylinder(h=4,d=30,center=true);
                    translate([4,0,36])
                    rotate([0,90,0])
                    cylinder(h=4,d=23,center=true);
                }
            }
            translate([4,0,27])
            rotate([0,90,0])
            cylinder(h=3,d=8,center=true);
        }
        //cutouts
        union()
        {
            translate([2,0,27])
            rotate([0,90,0])
            cylinder(h=6,d=3);
//            translate([4,0,27])
//            rotate([0,90,0])
//            cylinder(h=6,d=6);
            translate([-3,0,37])
            cube([10,11,9],center=true);
            translate([1,5,39])
            cube([10,20,2.7],center=true);
        }
    }
}