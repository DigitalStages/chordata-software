#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    ofSetFrameRate(30);
    ofSetVerticalSync(true);
    ofEnableSmoothing();
    bFill	= true;
    // Image setup
    imgChoice = 0;
    ratioH = ofGetHeight();
    fLoaded = false;
    sLoaded = false;
    // Viewport setup
    width = ofGetWidth();
    height = ofGetHeight();
    tglFull = false;
    // Viewport Front
    viewportFront.x = 20;
    viewportFront.y = 20;
    viewportFront.width = width/2 - 40;
    viewportFront.height = height - 40;
    // Viewport Side
    viewportSide.x = width/2 - 20;
    viewportSide.y = 20;
    viewportSide.width = width/2 - 40;
    viewportSide.height = height - 40;
    // Front View
    frontView.enableOrtho();
    frontView.removeAllInteractions();
    frontView.disableMouseMiddleButton();
    frontView.setNearClip(-2000000);
    frontView.setFarClip(2000000);
    //frontView.setVFlip(true);
    // Add needed mouse navigation actions in combination with shortkeys
    frontView.addInteraction(ofEasyCam::TRANSFORM_TRANSLATE_Z, OF_MOUSE_BUTTON_LEFT, OF_KEY_SHIFT);
    frontView.addInteraction(ofEasyCam::TRANSFORM_TRANSLATE_XY, OF_MOUSE_BUTTON_LEFT);
    frontView.addInteraction(ofEasyCam::TRANSFORM_TRANSLATE_XY, OF_MOUSE_BUTTON_RIGHT, OF_KEY_SHIFT);
    frontView.addInteraction(ofEasyCam::TRANSFORM_TRANSLATE_Z, OF_MOUSE_BUTTON_RIGHT);

    // Side View
    sideView.enableOrtho();
    sideView.removeAllInteractions();
    sideView.disableMouseMiddleButton();
    sideView.setNearClip(-1000000);
    sideView.setFarClip(1000000);
    //sideView.setVFlip(true);
    // Add needed mouse navigation actions in combination with shortkeys
    sideView.addInteraction(ofEasyCam::TRANSFORM_TRANSLATE_Z, OF_MOUSE_BUTTON_LEFT, OF_KEY_SHIFT);
    sideView.addInteraction(ofEasyCam::TRANSFORM_TRANSLATE_XY, OF_MOUSE_BUTTON_LEFT);
    sideView.addInteraction(ofEasyCam::TRANSFORM_TRANSLATE_XY, OF_MOUSE_BUTTON_RIGHT, OF_KEY_SHIFT);
    sideView.addInteraction(ofEasyCam::TRANSFORM_TRANSLATE_Z, OF_MOUSE_BUTTON_RIGHT);

    // Graphics
    // Cirle size and outline thickness
    size = 4;
    thickness = 2;
    // Colors
    colorIdle.set(255, 255, 255);
    colorOff.set(80, 80, 80);
    colorSelect.set(0, 255, 255);
    colorL.set(0, 255, 0);
    colorR.set(255, 255, 0);
    // Text
    zekton.load("verdana.ttf", 14, false);
    zekton.setLineHeight(18.0f);
    zekton.setLetterSpacing(1.037);
    colorC.set(255, 0, 0);
    // Set default coordinates for reference graphics
    refCube.setLineWidth(thickness);
    bActive = true;
    showLines = true;
    // Initiate default position values of reference cube
    LeftFrontBottom.set(87, -299, 78, -264);
    LeftFrontTop.set(87, -115, 77, -100);
    LeftBackBottom.set(77, -264, -84, -264);
    LeftBackTop.set(77, -100, -84, -101);
    RightFrontBottom.set(-95, -299, 87, -298);
    RightFrontTop.set(-95, -114, 87, -115);
    RightBackBottom.set(-85, -264, -95, -299);
    RightBackTop.set(-84, -100, -94, -115);
    // Add reference points of cube to vector array
    cube.push_back(LeftFrontBottom);
    cube.push_back(LeftFrontTop);
    cube.push_back(LeftBackBottom);
    cube.push_back(LeftBackTop);
    cube.push_back(RightFrontBottom);
    cube.push_back(RightFrontTop);
    cube.push_back(RightBackBottom);
    cube.push_back(RightBackTop);
    // Add labels to vector array
    cLbls.push_back("LeftFrontBottom");
    cLbls.push_back("LeftFrontTop");
    cLbls.push_back("LeftBackBottom");
    cLbls.push_back("LeftBackTop");
    cLbls.push_back("RightFrontBottom");
    cLbls.push_back("RightFrontTop");
    cLbls.push_back("RightBackBottom");
    cLbls.push_back("RightBackTop");
    // Set current cube reference
    curRef = 0;
    // Set reference distance of cube in millimeters
    refDist = 500;
    // Initiate default position values of joints
    Hips.set(-4, 70, -22);
    Spine.set(-4, 99, -20);
    Chest.set(-4, 155, -19);
    Neck.set(-4, 221, -22);
    Head.set(-4, 262, -8);
    HeadEnd.set(-4, 325, -16);
    LeftUpLeg.set(46, 43, -10);
    LeftLeg.set(38, -110, -15);
    LeftFoot.set(39, -259, -25);
    LeftFootEnd.set(41, -290, 16);
    RightUpLeg.set(-50, 43, -10);
    RightLeg.set(-42, -110, -15);
    RightFoot.set(-43, -259, -25);
    RightFootEnd.set(-45, -290, 16);
    LeftShoulder.set(8, 211, 4);
    LeftArm.set(56, 208, -8);
    LeftForeArm.set(71, 114, -26);
    LeftHand.set(85, 15, -4);
    LeftHandEnd.set(88, -18, 0);
    RightShoulder.set(-12, 211, 4);
    RightArm.set(-60, 208, -8);
    RightForeArm.set(-75, 114, -26);
    RightHand.set(-89, 15, -4);
    RightHandEnd.set(-92, -18, 0);
    // Add reference points of joints to vector array
    joints.push_back(Hips);
    joints.push_back(Spine);
    joints.push_back(Chest);
    joints.push_back(Neck);
    joints.push_back(Head);
    joints.push_back(HeadEnd);
    joints.push_back(LeftUpLeg);
    joints.push_back(LeftLeg);
    joints.push_back(LeftFoot);
    joints.push_back(LeftFootEnd);
    joints.push_back(RightUpLeg);
    joints.push_back(RightLeg);
    joints.push_back(RightFoot);
    joints.push_back(RightFootEnd);
    joints.push_back(LeftShoulder);
    joints.push_back(LeftArm);
    joints.push_back(LeftForeArm);
    joints.push_back(LeftHand);
    joints.push_back(LeftHandEnd);
    joints.push_back(RightShoulder);
    joints.push_back(RightArm);
    joints.push_back(RightForeArm);
    joints.push_back(RightHand);
    joints.push_back(RightHandEnd);
    // Add joint labels to vector array
    jLbls.push_back("Hips");
    jLbls.push_back("Spine");
    jLbls.push_back("Chest");
    jLbls.push_back("Neck");
    jLbls.push_back("Head");
    jLbls.push_back("HeadEnd");
    jLbls.push_back("LeftUpLeg");
    jLbls.push_back("LeftLeg");
    jLbls.push_back("LeftFoot");
    jLbls.push_back("LeftFootEnd");
    jLbls.push_back("RightUpLeg");
    jLbls.push_back("RightLeg");
    jLbls.push_back("RightFoot");
    jLbls.push_back("RightFootEnd");
    jLbls.push_back("LeftShoulder");
    jLbls.push_back("LeftArm");
    jLbls.push_back("LeftForeArm");
    jLbls.push_back("LeftHand");
    jLbls.push_back("LeftHandEnd");
    jLbls.push_back("RightShoulder");
    jLbls.push_back("RightArm");
    jLbls.push_back("RightForeArm");
    jLbls.push_back("RightHand");
    jLbls.push_back("RightHandEnd");
    // Set current joint number
    curJoint = 0;
    // Toggle mouse position
    getPosF = false;
    getPosS = false;
    // Toggle area
    areaF = false;
    areaS = false;
    // Toggle moving of current circle
    moveIt = false;
    // Toggle help text
    bHelp = false;
    bGuide = false;
    bAbout = false;
}

//--------------------------------------------------------------
void ofApp::update(){
    ofBackground(40);
    width = ofGetWidth();
    height = ofGetHeight();
    viewportFront.x = 20;
    viewportFront.y = 20;
    viewportFront.width = width/2 - 40;
    viewportFront.height = height - 40;
    viewportSide.x = width/2 + 20;
    viewportSide.y = 20;
    viewportSide.width = width/2 - 40;
    viewportSide.height = height - 40;
    offsetView = width/2;
    if(ofGetMouseX() < width / 2)
    {
        areaF = true;
        areaS = false;
    }
    else
    {
        areaF = false;
        areaS = true;
    }
}

//--------------------------------------------------------------
void ofApp::draw(){
    // Disable filling of shapes
    ofNoFill();
    // Dedicate line thickness of outlines
    ofSetLineWidth(thickness);
    // Set color
    ofSetColor(colorIdle);
    // FRONT VIEW
    drawViewportOutline(viewportFront);
    frontView.begin(viewportFront);
    // Content of Front View
    // Loaded image
    if(getPosF == true)
    {
        mouse = frontView.screenToWorld({ofGetMouseX(), ofGetMouseY(), 1.f});
        cout << int(mouse.x) << endl;
        cout << int(mouse.y) << endl;
        getPosF = false;
    }
    // If activate, show loaded image
    if(fLoaded == true)
    {
        front.draw(-front.getWidth()/2, -front.getHeight()/2);
    }
    // Draw circles of reference cube
    for(int i = 0; i < int(cube.size()); i++)
    {
        if(bActive == true)
        {
            if(i == curRef && areaF == true)
            {
                ofSetColor(colorSelect);
                if(moveIt == false)
                {
                    curPos = frontView.screenToWorld({ofGetMouseX(), ofGetMouseY(), 1.f});
                }
                else if(moveIt == true)
                {
                    // Move current cube reference circle on front view
                    newPos = frontView.screenToWorld({ofGetMouseX(), ofGetMouseY(), 1.f});
                    cube[i].x = cube[i].x + (newPos.x - curPos.x);
                    cube[i].y = cube[i].y + (newPos.y - curPos.y);
                    curPos.x = newPos.x;
                    curPos.y = newPos.y;
                }
            }
            else
            {
                ofSetColor(colorIdle);
            }
        }
        else if(bActive == false)
        {
            ofSetColor(colorOff);
        }
        refCube.circle(cube[i].x, cube[i].y, size);
    }
    //
    for(int k = 0; k < int(joints.size()); k++)
    {
        if(bActive == false)
        {
            if(k == curJoint && areaF == true)
            {
                ofSetColor(colorSelect);
                if(moveIt == false)
                {
                    curPos = frontView.screenToWorld({ofGetMouseX(), ofGetMouseY(), 1.f});
                }
                else if(moveIt == true)
                {
                    // Move current cube reference circle on front view
                    newPos = frontView.screenToWorld({ofGetMouseX(), ofGetMouseY(), 1.f});
                    joints[k].x = joints[k].x + (newPos.x - curPos.x);
                    joints[k].y = joints[k].y + (newPos.y - curPos.y);
                    curPos.x = newPos.x;
                    curPos.y = newPos.y;
                }
            }
            else
            {
                ofSetColor(colorIdle);
            }
        }
        else if(bActive == true)
        {
            ofSetColor(colorOff);
        }
        skeleton.circle(joints[k].x, joints[k].y, size);
        // Draw lines, if activated
        if(showLines)
        {
            // Draw all lines between joints to visualize the skeleton
            ofSetColor(colorC);
            skeleton.line(joints[0].x, joints[0].y, joints[1].x, joints[1].y);
            skeleton.line(joints[1].x, joints[1].y, joints[2].x, joints[2].y);
            skeleton.line(joints[2].x, joints[2].y, joints[3].x, joints[3].y);
            skeleton.line(joints[3].x, joints[3].y, joints[4].x, joints[4].y);
            skeleton.line(joints[4].x, joints[4].y, joints[5].x, joints[5].y);
            ofSetColor(colorL);
            skeleton.line(joints[0].x, joints[0].y, joints[6].x, joints[6].y);
            skeleton.line(joints[6].x, joints[6].y, joints[7].x, joints[7].y);
            skeleton.line(joints[7].x, joints[7].y, joints[8].x, joints[8].y);
            skeleton.line(joints[8].x, joints[8].y, joints[9].x, joints[9].y);
            ofSetColor(colorR);
            skeleton.line(joints[0].x, joints[0].y, joints[10].x, joints[10].y);
            skeleton.line(joints[10].x, joints[10].y, joints[11].x, joints[11].y);
            skeleton.line(joints[11].x, joints[11].y, joints[12].x, joints[12].y);
            skeleton.line(joints[12].x, joints[12].y, joints[13].x, joints[13].y);
            ofSetColor(colorL);
            skeleton.line(joints[2].x, joints[2].y, joints[14].x, joints[14].y);
            skeleton.line(joints[14].x, joints[14].y, joints[15].x, joints[15].y);
            skeleton.line(joints[15].x, joints[15].y, joints[16].x, joints[16].y);
            skeleton.line(joints[16].x, joints[16].y, joints[17].x, joints[17].y);
            skeleton.line(joints[17].x, joints[17].y, joints[18].x, joints[18].y);
            ofSetColor(colorR);
            skeleton.line(joints[2].x, joints[2].y, joints[19].x, joints[19].y);
            skeleton.line(joints[19].x, joints[19].y, joints[20].x, joints[20].y);
            skeleton.line(joints[20].x, joints[20].y, joints[21].x, joints[21].y);
            skeleton.line(joints[21].x, joints[21].y, joints[22].x, joints[22].y);
            skeleton.line(joints[22].x, joints[22].y, joints[23].x, joints[23].y);
            ofSetColor(colorIdle);
        }
    }
    // End viewport front
    frontView.end();

    // SIDE VIEW ====================================================================
    drawViewportOutline(viewportSide);
    sideView.begin(viewportSide);

    // Loaded image
    if(sLoaded == true)
    {
        side.draw(-side.getWidth()/2, -side.getHeight()/2);
    }

    // Draw circles of reference cube
    for(int j = 0; j < int(cube.size()); j++)
    {
        if(bActive == true)
        {
            if(j == curRef && areaS == true)
            {
                ofSetColor(colorSelect);
                if(moveIt == false)
                {
                    curPos = sideView.screenToWorld({ofGetMouseX(), ofGetMouseY(), 1.f});
                }
                else if(moveIt == true)
                {
                    // Move current cube reference circle on front view
                    newPos = sideView.screenToWorld({ofGetMouseX(), ofGetMouseY(), 1.f});
                    cube[j].z = cube[j].z + (newPos.x - curPos.x);
                    cube[j].w = cube[j].w + (newPos.y - curPos.y);
                    curPos.x = newPos.x;
                    curPos.y = newPos.y;
                }
            }
            else
            {
                ofSetColor(colorIdle);
            }
        }
        else if(bActive == false)
        {
            ofSetColor(colorOff);
        }
        refCube.circle(cube[j].z, cube[j].w, size);
    }
    // Draw circles of joints
    for(int l = 0; l < int(joints.size()); l++)
    {
        if(bActive == false)
        {
            if(l == curJoint && areaS == true)
            {
                ofSetColor(colorSelect);
                if(moveIt == false)
                {
                    curPos = sideView.screenToWorld({ofGetMouseX(), ofGetMouseY(), 1.f});
                }
                else if(moveIt == true)
                {
                    // Move current cube reference circle on front view
                    newPos = sideView.screenToWorld({ofGetMouseX(), ofGetMouseY(), 1.f});
                    joints[l].z = joints[l].z + (newPos.x - curPos.x);
                    joints[l].y = joints[l].y + (newPos.y - curPos.y);
                    curPos.x = newPos.x;
                    curPos.y = newPos.y;
                }
            }
            else
            {
                ofSetColor(colorIdle);
            }
        }
        else if(bActive == true)
        {
            ofSetColor(colorOff);
        }
        skeleton.circle(joints[l].z, joints[l].y, size);
        // If showLines active the lines representing the skeleton will be drawn
        if(showLines)
        {
            // Draw all lines between joints to visualize the skeleton
            ofSetColor(colorC);
            skeleton.line(joints[0].z, joints[0].y, joints[1].z, joints[1].y);
            skeleton.line(joints[1].z, joints[1].y, joints[2].z, joints[2].y);
            skeleton.line(joints[2].z, joints[2].y, joints[3].z, joints[3].y);
            skeleton.line(joints[3].z, joints[3].y, joints[4].z, joints[4].y);
            skeleton.line(joints[4].z, joints[4].y, joints[5].z, joints[5].y);
            ofSetColor(colorL);
            skeleton.line(joints[0].z, joints[0].y, joints[6].z, joints[6].y);
            skeleton.line(joints[6].z, joints[6].y, joints[7].z, joints[7].y);
            skeleton.line(joints[7].z, joints[7].y, joints[8].z, joints[8].y);
            skeleton.line(joints[8].z, joints[8].y, joints[9].z, joints[9].y);
            ofSetColor(colorR);
            skeleton.line(joints[0].z, joints[0].y, joints[10].z, joints[10].y);
            skeleton.line(joints[10].z, joints[10].y, joints[11].z, joints[11].y);
            skeleton.line(joints[11].z, joints[11].y, joints[12].z, joints[12].y);
            skeleton.line(joints[12].z, joints[12].y, joints[13].z, joints[13].y);
            ofSetColor(colorL);
            skeleton.line(joints[2].z, joints[2].y, joints[14].z, joints[14].y);
            skeleton.line(joints[14].z, joints[14].y, joints[15].z, joints[15].y);
            skeleton.line(joints[15].z, joints[15].y, joints[16].z, joints[16].y);
            skeleton.line(joints[16].z, joints[16].y, joints[17].z, joints[17].y);
            skeleton.line(joints[17].z, joints[17].y, joints[18].z, joints[18].y);
            ofSetColor(colorR);
            skeleton.line(joints[2].z, joints[2].y, joints[19].z, joints[19].y);
            skeleton.line(joints[19].z, joints[19].y, joints[20].z, joints[20].y);
            skeleton.line(joints[20].z, joints[20].y, joints[21].z, joints[21].y);
            skeleton.line(joints[21].z, joints[21].y, joints[22].z, joints[22].y);
            skeleton.line(joints[22].z, joints[22].y, joints[23].z, joints[23].y);
            ofSetColor(colorIdle);
        }
    }

    // Get current coordinates of mouse position in world view
    if(getPosS == true)
    {
        mouse = sideView.screenToWorld({ofGetMouseX(), ofGetMouseY(), 1.f});
        cout << int(mouse.x) << endl;
        cout << int(mouse.y) << endl;
        getPosS = false;
    }
    // End viewport side
    sideView.end();

    // Draw help message
    ofDrawBitmapString("Chordata - Sizer >> Press \"h\" to show 'Help' panel >> Press \"g\" to show 'Guide' panel >> Press \"a\" to show 'About' panel" , 20, 15);

    // Draw labels
    if(areaF == true)
    {
        if(bActive == true)
        {
            ofSetColor(colorSelect);
            zekton.drawString(cLbls[curRef], 30, height - 30);
        }
        else if(bActive == false)
        {
            ofSetColor(colorSelect);
            zekton.drawString(jLbls[curJoint], 30, height - 30);
        }
    }
    else if(areaS == true)
    {
        if(bActive == true)
        {
            ofSetColor(colorSelect);
            zekton.drawString(cLbls[curRef], width/2 + 30, height - 30);
        }
        else if(bActive == false)
        {
            ofSetColor(colorSelect);
            zekton.drawString(jLbls[curJoint], width/2 + 30, height - 30);
        }
    }
    // If "h" is pressed, help text will appear
    if (bHelp) {
        ofFill();
        ofSetColor(30, 30, 30, 200);
        ofDrawRectangle(30, 40, 500, 380);
        ofSetColor(240);
        stringstream ss;
        ss << "Chordata Sizer >> Help" << endl;
        ss << endl;
        ss << "This panel shows keys and navigation combinations" << endl;
        ss << endl;
        ss << "KEY MAP" << endl;
        ss << endl;
        ss << "'h' >> Shows this panel" << endl;
        ss << "'g' >> Shows a 'How to' guide" << endl;
        ss << "'a' >> Shows software info" << endl;
        ss << "'1' >> Load front image" << endl;
        ss << "'2' >> Load side image (right side of performer)" << endl;
        ss << "'c' >> Toggle lock-down of cube / skeleton" << endl;
        ss << "'e' >> Exports joint and reference points as XML file" << endl;
        ss << "'i' >> Imports a performer file" << endl;
        ss << "'f' >> Toggle fullscreen" << endl;
        ss << endl;
        ss << "NAVIGATION" << endl;
        ss << endl;
        ss << "ARROW UP / ARROW DOWN >> Scroll through reference points" << endl;
        ss << "SHIFT + LMB >> Pan view" << endl;
        ss << "SHIFT + RMB >> Zoom view" << endl;
        ss << "CTRL (CMD) + Mouse >> Move selected reference point" << endl;
        ss << "LMB Double click >> View jumps to default" << endl;
        ss << endl;
        ofDrawBitmapString(ss.str().c_str(), 50, 60);
    }
    // If "g" is pressed, guide text will appear
    if (bGuide) {
        ofFill();
        ofSetColor(30, 30, 30, 200);
        ofDrawRectangle(30, 40, 970, 510);
        ofSetColor(240);
        stringstream ss;
        ss << "Chordata Sizer >> Guide" << endl;
        ss << endl;
        ss << "The following guide will show how to use this software to create a referenced skeleton of a performer" << endl;
        ss << "to be used in Chordata Mover" << endl;
        ss << endl;
        ss << "1. Load reference images of performer" << endl;
        ss << "    >> Move mouse cursor over the left area and press '1' >> Select the front image file and press ok"<< endl;
        ss << "    >> Move mouse cursor over the right area and press '2' >> Select the side image file and press ok"<< endl;
        ss << endl;
        ss << "2. Set set reference points of cube (The default cube is 50 cm each length) to calculate size ratio" << endl;
        ss << "    >> Use 'Arrow Up' and 'Arrow Down' keys to scroll through all reference points" << endl;
        ss << "    >> Position each point according to the uploaded image by pressing CTRL or CMD, while using the mouse/pad"<< endl;
        ss << "    >> Use 'SHIFT + LMB' to pan, and 'SHIFT + RMB' to zoom, so it is easy to position the reference points correctly"<< endl;
        ss << endl;
        ss << "3. Set set reference points of cube (The default cube is 50 cm each length) to calculate size ratio" << endl;
        ss << "    >> Use 'Arrow Up' and 'Arrow Down' keys to scroll through all reference points" << endl;
        ss << "    >> Position each point according to the uploaded image by pressing CTRL or CMD, while using the mouse/pad"<< endl;
        ss << "    >> Use SHIFT + LMB to pan, and SHIFT + RMB to zoom, so it is easy to position the reference points correctly"<< endl;
        ss << "    >> After all reference points are positioned for the front image, continue with the side image"<< endl;
        ss << endl;
        ss << "4. Set set joint points of skeleton (Using markers on the performer before taking pictures will help here)" << endl;
        ss << "    >> Press 'c' in order to lock the reference cube and unlock the skeleton" << endl;
        ss << "    >> Use 'Arrow Up' and 'Arrow Down' keys to scroll through all joint references" << endl;
        ss << "    >> Position each point according to the uploaded image by pressing CTRL or CMD, while using the mouse/pad"<< endl;
        ss << "    >> Use SHIFT + LMB to pan, and SHIFT + RMB to zoom, so it is easy to position the joint references correctly"<< endl;
        ss << "    >> After all reference points are positioned for the front image, continue with the side image"<< endl;
        ss << endl;
        ss << "5. Export / Import XML file" << endl;
        ss << "    >> If all joint references are set, press 'e' to export the skeleton / cube data to a XML file" << endl;
        ss << "    >> The software will use the cubes reference to create a ratio, which will be used to save all distances in mm" << endl;
        ss << "    >> The XML file can then imported in 'Chordata Mover', or similar softwares"<< endl;
        ss << "    >> If a performer file needs to be modified, press 'i' to import an existing file"<< endl;
        ss << endl;
        ss << "If there are any questions or bugs, please send a message to contact@jensmeisner.net" << endl;
        ss << "Thank you!" << endl;
        ss << endl;
        ofDrawBitmapString(ss.str().c_str(), 50, 60);
    }
    // If "a" is pressed, guide text will appear
    if (bAbout) {
        ofFill();
        ofSetColor(30, 30, 30, 200);
        ofDrawRectangle(40, 40, 500, 130);
        ofSetColor(240);
        stringstream ss;
        ss << "Chordata Sizer >> About" << endl;
        ss << endl;
        ss << "Version v0.1" << endl;
        ss << endl;
        ss << "Software developed by Jens Meisner (www.jensmeisner.net)" << endl;
        ss << endl;
        ss << "Licensed under GNU GENERAL PUBLIC LICENSE 3.0" << endl;
        ss << endl;
        ofDrawBitmapString(ss.str().c_str(), 60, 60);
    }
}


//--------------------------------------------------------------
void ofApp::drawViewportOutline(const ofRectangle & viewport){
    ofPushStyle();
    ofFill();
    ofSetColor(50);
    ofSetLineWidth(0);
    ofDrawRectangle(viewport);
    ofNoFill();
    ofSetColor(25);
    ofSetLineWidth(1.0f);
    ofDrawRectangle(viewport);
    ofPopStyle();
}


// Load images dialog ------------------------------------------------
void ofApp::processOpenFileSelection(ofFileDialogResult openFileResult){
    // Get name and path of the file to be open
    ofLogVerbose("getName(): "  + openFileResult.getName());
    ofLogVerbose("getPath(): "  + openFileResult.getPath());
    //Set the file call
    ofFile file (openFileResult.getPath());
    // Proceed loading file, if file exists
    if (file.exists()){
        ofLogVerbose("The file exists - now checking the type via file extension");
        string fileExtension = ofToUpper(file.getExtension());
        // Check if file has jpg or png extension
        if (fileExtension == "JPG" || fileExtension == "PNG") {
            // Save the file extension to use when we save out
            originalFileExtension = fileExtension;
            // Load the selected image as front image
            if(imgChoice == 1)
            {
                // Load image file
                front.load(openFileResult.getPath());
                // Check if image is bigger or smaller than screen size
                if (front.getHeight() > ratioH || front.getHeight() < ratioH)
                {
                    // Calculate ratio and resize the image accordingly
                    if(front.getHeight() > ratioH)
                    {
                        scr2imgRatio = front.getHeight() / ratioH;
                        front.resize(front.getWidth() / scr2imgRatio, ratioH);
                    }
                    else if(front.getHeight() < height)
                    {
                        scr2imgRatio = ratioH / front.getHeight();
                        front.resize(front.getWidth() * scr2imgRatio, ratioH);
                    }
                    else
                    {
                        front.resize(ratioH, ratioH);
                    }
                }
                // Image is loaded
                fLoaded = true;
            }
            // Load the selected image as side image
            else if(imgChoice == 2)
            {
                // Load image file
                side.load(openFileResult.getPath());
                // Check if image is bigger or smaller than screen size
                if (side.getHeight() > ratioH || side.getHeight() < ratioH)
                {
                    // Calculate ratio and resize the image accordingly
                    if(side.getHeight() > ratioH)
                    {
                        scr2imgRatio = side.getHeight() / ratioH;
                        side.resize(side.getWidth() / scr2imgRatio, ratioH);
                    }
                    else if(side.getHeight() < ratioH)
                    {
                        scr2imgRatio = ratioH / side.getHeight();
                        side.resize(side.getWidth() * scr2imgRatio, ratioH);
                    }
                    else
                    {
                        side.resize(ratioH, ratioH);
                    }
                }
                sLoaded = true;
            }
        }
        if(fileExtension == "XML")
        {
            // Save the file extension to use when we save out
            originalFileExtension = fileExtension;
            // Load xml file
            XML.loadFile(openFileResult.getPath());
        }
    }
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
    if (key == '1'){
        imgChoice = 1;
        // Open the Open File Dialog
        ofFileDialogResult openFileResult= ofSystemLoadDialog("Select a jpg or png");
        // Check if the user opened a file
        if (openFileResult.bSuccess){
            ofLogVerbose("User selected a file");
            // We have a file, check it and process it
            processOpenFileSelection(openFileResult);
        }else {
            ofLogVerbose("User hit cancel");
        }
    }
    if (key == '2'){
        imgChoice = 2;
        // Open the Open File Dialog
        ofFileDialogResult openFileResult= ofSystemLoadDialog("Select a jpg or png");
        // Check if the user opened a file
        if (openFileResult.bSuccess){
            ofLogVerbose("User selected a file");
            // We have a file, check it and process it
            processOpenFileSelection(openFileResult);
        }else {
            ofLogVerbose("User hit cancel");
        }
    }
    if (key == 'e' || key == 'E'){
        // Get hips position to substract it resulting in hips as 0, 0, 0
        offset.set(joints[0]);
        // Get reference data of x and y via front image reference cube (500 millimeter)
        // Calcualte mean of upper and lowever reference points
        fBtmX = (cube[0].x + cube[2].x + cube[4].x + cube[6].x) / 4;
        fBtmY = (cube[0].y + cube[2].y + cube[4].y + cube[6].y) / 4;
        fTopX = (cube[1].x + cube[3].x + cube[5].x + cube[7].x) / 4;
        fTopY = (cube[1].y + cube[3].y + cube[5].y + cube[7].y) / 4;
        fRightX = (cube[4].x + cube[5].x + cube[6].x + cube[7].x) / 4;
        fRightY = (cube[4].y + cube[5].y + cube[6].y + cube[7].y) / 4;
        fLeftX = (cube[0].x + cube[1].x + cube[2].x + cube[3].x) / 4;
        fLeftY = (cube[0].y + cube[1].y + cube[2].y + cube[3].y) / 4;
        // Get distance between upper and lower mean
        fDistY = ofDist(fBtmX, fBtmY, fTopX, fTopY);
        fDistX = ofDist(fLeftX, fLeftY, fRightX, fRightY);
        // Calculate mean of front and back reference points
        sFrontZ = (cube[0].z + cube[1].z + cube[4].z + cube[5].z) / 4;
        sFrontY = (cube[0].w + cube[1].w + cube[4].w + cube[5].w) / 4;
        sBackZ = (cube[2].z + cube[3].z + cube[6].z + cube[7].z) / 4;
        sBackY = (cube[2].w + cube[3].w + cube[6].w + cube[7].w) / 4;
        // Get distance between upper and lower mean
        sDistZ = ofDist(sFrontZ, sFrontY, sBackZ, sBackY);
        // Calculate reference ratio of xy (front image) and z (side image)
        refRatioX = refDist / fDistX;
        refRatioY = refDist / fDistY;
        refRatioZ = refDist / sDistZ;
        // Write all joint positions and cube references in converted millimeters into a XML file and save it
        ofFileDialogResult saveFileResult = ofSystemSaveDialog(ofGetTimestampString() + "." + ofToLower(originalFileExtension), "Save your performer file");
        if (saveFileResult.bSuccess){
            // Clear out XML file
            XML.clear();
            // Add Root tag
            XML.addTag("SKELETON");
            XML.pushTag("SKELETON");
            // Go through all joint references and add x, y, z to added joint name
            for(int i = 0; i < int(joints.size()); i++)
            {
                XML.addTag(jLbls[i]);
                XML.pushTag(jLbls[i]);
                XML.addValue("X", (joints[i].x - offset.x) * refRatioX);
                XML.addValue("Y", (joints[i].y - offset.y) * refRatioY);
                XML.addValue("Z", (joints[i].z - offset.z) * refRatioZ);
                XML.popTag();
            }
            XML.popTag();
            // Add reference cube
            XML.addTag("CUBE");
            XML.pushTag("CUBE");
            // Go through all cube references and add x, y of front imager, y of side image, z (x of side image)
            for(int i = 0; i < int(cube.size()); i++)
            {
                XML.addTag(cLbls[i]);
                XML.pushTag(cLbls[i]);
                XML.addValue("X", (cube[i].x - offset.x) * refRatioX);
                XML.addValue("Y", (cube[i].y - offset.y) * refRatioY);
                XML.addValue("Z", (cube[i].z - offset.z) * refRatioZ);
                XML.addValue("Y2", (cube[i].w - offset.y) * refRatioY);
                XML.popTag();
            }
            // Add ratios for import capability
            XML.popTag();
            XML.addTag("RATIO");
            XML.pushTag("RATIO");
            XML.addValue("X", refRatioX);
            XML.addValue("Y", refRatioY);
            XML.addValue("Z", refRatioZ);
            XML.popTag();
            XML.popTag();
            XML.addTag("OFFSET");
            XML.pushTag("OFFSET");
            XML.addValue("X", offset.x);
            XML.addValue("Y", offset.y);
            XML.addValue("Z", offset.z);
            XML.popTag();
            // Save all to the file
            XML.saveFile(saveFileResult.filePath);
        }
    }
    if (key == 'i' || key == 'I'){
        // Open the Open File Dialog
        ofFileDialogResult openFileResult= ofSystemLoadDialog("Select a performer xml file");
        // Check if the user opened a file
        if (openFileResult.bSuccess){
            ofLogVerbose("User selected a file");
            // We have a file, check it and process it
            processOpenFileSelection(openFileResult);
            // Get ratios for imported file
            XML.pushTag("OFFSET");
            offset.x = XML.getValue("X", 0.0);
            offset.y = XML.getValue("Y", 0.0);
            offset.z = XML.getValue("Z", 0.0);
            XML.popTag();
            XML.pushTag("RATIO");
            refRatioX = XML.getValue("X", 0.0);
            refRatioY = XML.getValue("Y", 0.0);
            refRatioZ = XML.getValue("Z", 0.0);
            XML.popTag();
            XML.pushTag("SKELETON");
            // Go through all joint references and add x, y, z to added joint name
            for(int i = 0; i < int(joints.size()); i++)
            {
                XML.pushTag(jLbls[i]);
                joints[i].x = int(XML.getValue("X", 0.0) / refRatioX + offset.x);
                joints[i].y = int(XML.getValue("Y", 0.0)  / refRatioY + offset.y);
                joints[i].z = int(XML.getValue("Z", 0.0)  / refRatioZ + offset.z);
                XML.popTag();
            }
            XML.popTag();
            // Add reference cube
            XML.pushTag("CUBE");
            // Go through all cube references and add x, y of front imager, y of side image, z (x of side image)
            for(int i = 0; i < int(cube.size()); i++)
            {
                XML.pushTag(cLbls[i]);
                cube[i].x = int(XML.getValue("X", 0.0) / refRatioX + offset.x);
                cube[i].y = int(XML.getValue("Y", 0.0) / refRatioY + offset.y);
                cube[i].z = int(XML.getValue("Z", 0.0) / refRatioZ + offset.z);
                cube[i].w = int(XML.getValue("Y2", 0.0) / refRatioY + offset.y);
                XML.popTag();
            }
            XML.popTag();
        }else {
            ofLogVerbose("User hit cancel");
        }
    }
    if (key == 'h' || key == 'H')
    {
        // Switch on/off help text
        if(bHelp == false)
        {
            bHelp = true;
            if(bGuide == true)
            {
                bGuide = false;
            }
            else if(bAbout == true)
            {
                bAbout = false;
            }
        }
        else
        {
            bHelp = false;
        }
    }
    if (key == 'g' || key == 'G')
    {
        // Switch on/off help text
        if(bGuide == false)
        {
            bGuide = true;
            if(bHelp == true)
            {
                bHelp = false;
            }
            else if(bAbout == true)
            {
                bAbout = false;
            }
        }
        else
        {
            bGuide = false;
        }
    }
    if (key == 'a' || key == 'A')
    {
        // Switch on/off help text
        if(bAbout == false)
        {
            bAbout = true;
            if(bHelp == true)
            {
                bHelp = false;
            }
            else if(bGuide == true)
            {
                bGuide = false;
            }
        }
        else
        {
            bAbout = false;
        }
    }
    if (key == 'f' || key == 'F')
    {
        // Space key to toggle fullscreen
        ofToggleFullscreen();
    }
    if (key == 'c' || key == 'C')
    {
        // Enable skeleton / Disable cube and visa verse
        if(bActive == false)
        {
            bActive = true;
        }
        else
        {
            bActive = false;
        }
    }
    if (key == 'm' || key == 'M')
    {
        // Get current mouse position with world coordinates
        if (ofGetMouseX() < width / 2)
        {
            getPosF = true;
        }
        else
        {
            getPosS = true;
        }
    }
    if (key == OF_KEY_UP)
    {
        // Scroll through reference points upwards
        if(bActive == true)
        {
            curRef = curRef + 1;
            if (curRef > int(cube.size() - 1))
            {
                curRef = 0;
            }
        }
        if(bActive == false)
        {
            curJoint = curJoint + 1;
            if (curJoint > int(joints.size() - 1))
            {
                curJoint = 0;
            }
        }
    }
    if (key == OF_KEY_DOWN)
    {
        // Scroll through reference points downwards
        if(bActive == true)
        {
            curRef = curRef - 1;
            if (curRef < 0)
            {
                curRef = int(cube.size()) - 1;
            }

        }
        if(bActive == false)
        {
            curJoint = curJoint - 1;
            if (curJoint < 0)
            {
                curJoint = int(joints.size()) - 1;
            }
        }
    }
    if ((key == OF_KEY_CONTROL || key == OF_KEY_COMMAND) && moveIt == false)
    {
        // Edit current selected reference point
        moveIt = true;
    }
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){
    if ((key == OF_KEY_CONTROL || key == OF_KEY_COMMAND) && moveIt == true)
    {
        // Stop editing position of reference point
        moveIt = false;
    }
}


