#pragma once

#include "ofMain.h"
#include "ofxVectorGraphics.h"
#include "ofxXmlSettings.h"

class ofApp : public ofBaseApp{

public:
    void setup();
    void update();
    void draw();
    void keyPressed(int key);
    void keyReleased(int key);

    // Load images
    void processOpenFileSelection(ofFileDialogResult openFileResult);
    ofImage image, front, side;
    string originalFileExtension;
    int imgChoice, ratioH;
    bool fLoaded, sLoaded;
    // Viewports
    void drawViewportOutline(const ofRectangle & viewport);
    ofEasyCam frontView;
    ofEasyCam sideView;
    ofRectangle viewportFront;
    ofRectangle viewportSide;
    int width, height, offsetView;
    bool tglFull;
    // Mouse position
    ofVec2f curPos, newPos;
    // Joint positions in x, y, z
    ofVec3f Hips, Spine, Chest;
    ofVec3f LeftUpLeg, LeftLeg, LeftFoot, LeftFootEnd;
    ofVec3f RightUpLeg, RightLeg, RightFoot, RightFootEnd;
    ofVec3f LeftShoulder, LeftArm, LeftForeArm, LeftHand, LeftHandEnd;
    ofVec3f RightShoulder, RightArm, RightForeArm, RightHand, RightHandEnd;
    ofVec3f Neck, Head, HeadEnd;
    vector <ofVec3f> joints;
    int curJoint;
    // Reference cube
    ofVec4f LeftFrontBottom, LeftFrontTop, LeftBackBottom, LeftBackTop;
    ofVec4f RightFrontBottom, RightFrontTop, RightBackBottom, RightBackTop;
    vector <ofVec4f> cube;
    int curRef;
    float refDist, fBtmX, fBtmY, fTopX, fTopY, fRightX, fRightY, fLeftX, fLeftY, sFrontZ, sFrontY, sBackZ, sBackY;
    float refRatioX, refRatioY, refRatioZ, fDistX, fDistY, sDistZ;
    // Graphics::Skeleton
    ofxVectorGraphics skeleton;
    ofxVectorGraphics FHips, FSpine, FSpine1;
    ofxVectorGraphics FLeftUpLeg, FLeftLeg, FLeftFoot, FLeftFootEnd;
    ofxVectorGraphics FRightUpLeg, FRightLeg, FRightFoot, FRightFootEnd;
    ofxVectorGraphics FLeftShoulder, FLeftArm, FLeftForeArm, FLeftHand, FLeftHandEnd;
    ofxVectorGraphics FRightShoulder, FRightArm, FRightForeArm, FRightHand, FRightHandEnd;
    ofxVectorGraphics FNeck, FHead, FHeadEnd;
    ofxVectorGraphics SHips, SSpine, SSpine1;
    ofxVectorGraphics SUpLeg, SLeg, SFootHeel, SFootEnd;
    ofxVectorGraphics SShoulder, SArm, SForeArm, SHand, SHandEnd;
    ofxVectorGraphics SNeck, SHead, SHeadEnd;
    // Graphics::Reference cube
    ofxVectorGraphics refCube;
    bool bFill, bActive, showLines;
    int size;
    int thickness;
    ofColor colorIdle, colorSelect, colorOff, colorL, colorR, colorC;
    float imgRatio, scr2imgRatio;
    bool getPosF, getPosS, areaF, areaS, moveIt;
    glm::vec3 mouse;
    ofTrueTypeFont	zekton;
    vector <string> jLbls;
    vector <string> cLbls;
    // Help text, guide text, about text
    bool bHelp, bGuide, bAbout;
    // Export/Import XML file
    ofxXmlSettings XML;
    ofVec3f offset;
};
