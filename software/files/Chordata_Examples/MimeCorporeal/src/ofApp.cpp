#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    // GENERAL
    ofSetWindowTitle("Mime Corporeal (Rotation Example)");
    ofSetVerticalSync(false);
    ofDisableArbTex();
    ofEnableAntiAliasing();
    ofSetFrameRate(30);
    width = ofGetWidth();
    height = ofGetHeight();

    // FONT
    zekton.load("zekton.ttf", 32, true);
    zekton.setGlobalDpi(96);

    // CAMERA
    cam.setPosition(25, 17, 45);
    cam.lookAt(ofVec3f(0, 15, 0));
    cam.setFarClip(500);
    cam.setNearClip(.5f);
    //cam.setVFlip(true);
    // Modify mouse interactivity
    cam.removeAllInteractions();
    cam.addInteraction(ofEasyCam::TRANSFORM_TRANSLATE_XY, OF_MOUSE_BUTTON_LEFT, OF_KEY_SHIFT);
    cam.addInteraction(ofEasyCam::TRANSFORM_TRANSLATE_Z, OF_MOUSE_BUTTON_RIGHT, OF_KEY_SHIFT);
    cam.addInteraction(ofEasyCam::TRANSFORM_ROTATE, OF_MOUSE_BUTTON_LEFT);
    cam.addInteraction(ofEasyCam::TRANSFORM_TRANSLATE_XY, OF_MOUSE_BUTTON_RIGHT);

    // LIGHT
    //Make Light to a sun light or directional light
    light.setDirectional();

    // SHADOW
    fustrumSize = 400.0;
    farClip = 1000.0;
    // range of the shadow camera //
    shadowMap.setup(4096);
//    // Get materials from meshes to apply shadows to >> Not available for fbx imported meshes
//    for(int i=0; i < int(fbx1.getNumMeshes()); i++){
//        shadowMap.setupMaterialWithShadowMap(fbx1.);
//    }
    shadowMap.setupMaterialWithShadowMap(groundMaterial);
    listener = enableShadows.newListener([this](bool & on){
        if(!on){shadowMap.begin(light, fustrumSize, 1, farClip);
            shadowMap.end();
        }
    });

    // GUI
    // Colors for menu
    gC = ofColor(0, 165, 255);
    sC = ofColor(255, 165, 0);
    // Values that are used in connection with GUI
    bFull = false;
    portIn = 6568;
    pIn = ofToString(portIn);
    bGui = true;
    bLive = false;
    // Create menu
    panel = gui.addPanel("MENU - Press 'm'");
    panel->setPosition(10, 10);
    generalS = panel->addGroup("GENERAL SETTINGS ====");
    generalS->getHeader()->setBackgroundColor(gC);
    generalS->add(tglAbout.set("About", false));
    generalS->add(tglFull.set("Toggle Fullscreen", false));
    generalS->add(goLive.set("GO LIVE!", false));
    generalS->add<ofxGuiButton>(offsetBtn.set("Offset Bones", false));
    oscSettings = generalS->addGroup("Osc Options");
    oscSettings->add<ofxGuiIntInputField>(portInInput.set("Receiver Port:", 6568));
    oscSettings->minimize();
    sceneS = panel->addGroup("SCENE SETTINGS ========");
    sceneS->getHeader()->setBackgroundColor(sC);
    customChar = sceneS->addGroup("CHARACTER SETTINGS");
    customChar->getHeader()->setBackgroundColor(ofColor(68, 185, 120));
    customChar->setBackgroundColor(ofColor(168, 255, 120, 40));
    customChar->add(customFbx.set("Custom Character", false));
    customChar->add(tglTweak.set("Toggle Bone Tweak", false));
    customChar->add(boneTweak.set("Tweak Bone Rotation", ofVec3f(0, 0, 0), ofVec3f(-180, -180, -180), ofVec3f(180, 180, 180)));
    customChar->add<ofxGuiButton>(tweakBoneBtn.set("Tweak!", false));
    customChar->minimize();
    sceneS->add(showSphere.set("Show Sphere", true));
    sceneS->add(floorSize.set("Floor Size", ofVec2f(500, 500), ofVec2f(0, 0), ofVec2f(5000, 5000)));
    colorSettings = sceneS->addGroup("Color Options");
    colorSettings->add(colorBG.set("Background", ofColor(0, 147, 212), ofColor(0), ofColor(255)) );
    colorSettings->add(colorSphere.set("Sphere", ofColor(214, 214, 214), ofColor(0), ofColor(255)) );
    colorSettings->add(colorFloor.set("Floor", ofFloatColor(0.74, 0.41, 0.21), ofFloatColor(0.0, 0.0), ofFloatColor(1.0, 1.0)) );
    colorSettings->minimize();
    lightSettings = sceneS->addGroup("Light Options");
    lightSettings->add(colorLight.set("Color", ofColor(255, 255, 255), ofColor(0, 0), ofColor(255, 255)) );
    lightSettings->add(lightPos.set("Position", ofVec3f(-24, 90, 172), ofVec3f(-500, -500, -500), ofVec3f(500, 500, 500)));
    lightSettings->add(lightLookAt.set("Look At", ofVec3f(0, -30, 0), ofVec3f(-500, -500, -500), ofVec3f(500, 500, 500)));
    lightSettings->minimize();
    sceneS->add(enableShadows.set("Toggle Shadows", true));

    // FBX
    customFbx = false;
    // Set FBX scene unit to meter
    //ofxFBXSource::Scene::FbxUnits = FbxSystemUnit::m;
    // Import Default FBX Scene/Character
    ofxFBXSource::Scene::Settings settings;
    settings.filePath = "MimeCorporeal.fbx";
    settings.printInfo = true;
    // Load scene with characters from file
    if( fbx1.load(settings) ) {
        cout << "ofApp :: loaded the scene OK" << endl;
    } else {
        cout << "ofApp :: Error loading the scene" << endl;
    }
    fbx1.getSourceTextures();
    fbx1.setPosition( 0, 0, 0);
    fbx1.setMaterialsEnabled(true);
    fbx1.setAnimation(0);
    // Import Custom FBX Scene/Character
    ofxFBXSource::Scene::Settings settings2;
    //settings2.filePath = "StripesChar.fbx";
    settings2.filePath = "MimeCorporeal.fbx";
    settings2.printInfo = true;
    // Print all info of fbx scene to terminal
    //ofSetLogLevel(OF_LOG_VERBOSE);
    // Load scene with characters from file
    if( fbx2.load(settings2) ) {
        cout << "ofApp :: loaded the scene OK" << endl;
    } else {
        cout << "ofApp :: Error loading the scene" << endl;
    }
    fbx2.getSourceTextures();
    fbx2.setPosition( 0, 0, 0);
    fbx2.setMaterialsEnabled(true);
    fbx2.setAnimation(0);

    // DATA
    // Create vector with names of bones, which will be addressed by incoming data
    jLbls.push_back("Hips");            //0 >> CH_5_0
    jLbls.push_back("Spine");           //1
    jLbls.push_back("Chest");           //2 >> CH_2_1
    jLbls.push_back("Neck");            //3
    jLbls.push_back("Head");            //4 >> CH_2_2
    jLbls.push_back("HeadEnd");         //5
    jLbls.push_back("LeftUpLeg");       //6 >> CH_4_0
    jLbls.push_back("LeftLeg");         //7 >> CH_4_1
    jLbls.push_back("LeftFoot");        //8 >> CH_4_2
    jLbls.push_back("LeftFootEnd");     //9
    jLbls.push_back("RightUpLeg");      //10 >> CH_6_0
    jLbls.push_back("RightLeg");        //11 >> CH_6_1
    jLbls.push_back("RightFoot");       //12 >> CH_6_2
    jLbls.push_back("RightFootEnd");    //13
    jLbls.push_back("LeftShoulder");    //14
    jLbls.push_back("LeftArm");         //15 >> CH_1_0
    jLbls.push_back("LeftForeArm");     //16 >> CH_1_1
    jLbls.push_back("LeftHand");        //17 >> CH_1_2
    jLbls.push_back("LeftHandEnd");     //18
    jLbls.push_back("RightShoulder");   //19
    jLbls.push_back("RightArm");        //20 >> CH_3_0
    jLbls.push_back("RightForeArm");    //21 >> CH_3_1
    jLbls.push_back("RightHand");       //22 >> CH_3_2
    jLbls.push_back("RightHandEnd");    //23

    // Joint data
    jointCount = 24;
    quat.set(0, 0, 0, 1);
    hipsT.set(0, 360, 0);
    // Create vectors which manage offsets and incoming rotation data
    cout<<"MAKE SURE TO USE FOLLOWING NAMING CONVENTION FOR YOUR FBX CONTROL BONES!"<<endl;
    fbx1.getSkeletons();
    fbx1.getSkeletonInfo();
    for(int i = 0; i < jointCount; i++)
    {
        jRot.push_back(quat);
        ofQuaternion rot = fbx1.getBone(jLbls[i])->getOrientationQuat();
        jRot[i].set(rot);
        zeroOffset.push_back(quat);
        zeroOffset[i].set(jRot[i]);
        zeroDiff.push_back(quat);
        zeroDiff[i].set(quat);
        // Printing out names
        cout<<fbx1.getBone(jLbls[i])->getName()<<endl;
        // Debug
        //cout<<fbx1.getBone(jLbls[i])->getPosition()<<endl;
        //cout<<zOffset[i]<<endl;
    }
    calcRatio = true;
    bAbout = false;
    //Setup Thread getting rotation in quaternion via osc
    receiver.setup(portIn);
    boneTweak.set(ofVec3f(0, 0, 0));
}

//--------------------------------------------------------------
void ofApp::update(){
    //Update screen size
    width = ofGetWidth();
    height = ofGetHeight();
    // Update background color
    ofBackground(colorBG);
    lightP.set(lightPos);
    light.setPosition(lightP);
    lightLA.set(lightLookAt);
    light.lookAt(lightLA);
    lightC.set(colorLight);
    light.setDiffuseColor(lightC);
    // Start listening to osc messages
    if(goLive == true)
    {
        if(offsetBtn == true)
        {
            offsetGyros();
        }
        // Get Rotation Data from Chordata Mover Software via Osc
        receiveOsc();
        // Early update is getting animation information from fbx file itself
        if(customFbx == false)
            {
            // Calculate translation ratio from Chordata Mover Skeleton to Character
            if(calcRatio == true)
            {
                spaceRatio = fbx1.getBone(jLbls[0])->getPosition().y / hipsT.y;
                calcRatio = false;
                cout<<spaceRatio<<endl;
            }
            fbx1.earlyUpdate();
            // Update hips position
            fbx1.getBone(jLbls[0])->setPosition(hipsT.x * spaceRatio, hipsT.y * spaceRatio, hipsT.z * spaceRatio);
            // Update bone rotations
            for(int i = 0; i < jointCount; i++)
            {
                // Update relative rotation to offset
                fbx1.getBone(jLbls[i])->setOrientation(jRot[i].inverse() * zeroDiff[i]);
            }
            //Update meshes around new position of bones
            fbx1.lateUpdate();
        }
        else if(customFbx == true){
            // Calculate translation ratio from Chordata Mover Skeleton to Character
            if(calcRatio == true)
            {
                spaceRatio = fbx1.getBone(jLbls[0])->getPosition().y / hipsT.y;
                calcRatio = false;
                cout<<spaceRatio<<endl;
            }
            fbx2.earlyUpdate();
            // Update hips position
            fbx2.getBone(jLbls[0])->setPosition(hipsT.x * spaceRatio, hipsT.y * spaceRatio, hipsT.z * spaceRatio);
            // Update bone rotation
            for(int i = 0; i < jointCount; i++)
            {
                //Update bone rotation
                fbx2.getBone(jLbls[i])->setOrientation(jRot[i].inverse() * zeroDiff[i]);
                // Update relative rotation to offset
                if(tglTweak == true)
                {
                    setOrientationQuat(boneTweak);
                    cout<<boneTweak<<endl;
                    cout<<offsetDiff<<endl;
                    diff = offsetDiff * zeroOffset[curJoint].inverse();
                    quat = fbx2.getBone(jLbls[curJoint])->getOrientationQuat();
                    quat.inverse();
                    quat = quat * offsetDiff;
                    quat.normalize();
                    //Update meshes around new position of bones
                    fbx2.getBone(jLbls[curJoint])->setOrientation(quat);
                }
            }
            //Update meshes around new position of bones
            fbx2.lateUpdate();
        }
    }
    else if(goLive == false){
        calcRatio = true;
    }
    // Toggle fullscreen
    if(tglFull == true || bFull == true)
    {
        ofSetFullscreen(true);
    }
    else if(tglFull == true && bFull == false)
    {
        ofSetFullscreen(true);
        bFull = true;
    }
    else{
        ofSetFullscreen(false);
    }
    if(tweakBoneBtn && tglTweak == true)
    {
        zeroOffset[curJoint].set(diff);
        cout<<diff<<endl;
        offsetGyros();
        boneTweak.set(ofVec3f(0, 0, 0));
    }
}

//--------------------------------------------------------------
void ofApp::draw(){

    ofEnableDepthTest();
    // Calculate shadow
    if(enableShadows){
        shadowMap.begin(light, fustrumSize, 1, farClip);
        if(customFbx == false) { fbx1.drawMeshes(); }
        else if(customFbx == true) { fbx2.drawMeshes(); }
        shadowMap.end();
// Get materials from meshes to apply shadows to >> Not available for fbx imported meshes
//        for(size_t i=0; i < int(fbx.getNumMeshes()); i++){
//            shadowMap.updateMaterial(fbx.getMeshHelper(i).material);
//        }
        shadowMap.updateMaterial(groundMaterial);
    }
    // Enable light environment
    ofEnableLighting();
    ofEnableAlphaBlending();
    cam.begin();
    // Apply material to all following objects
    groundMaterial.begin();
    groundMaterial.setDiffuseColor(lightC);
    groundMaterial.setEmissiveColor(colorFloor);
    ofPushMatrix();
    // Rotate floor plane first
    ofRotateXDeg(90);
    // Draw floor plane
    ofDrawPlane(floorSize->x, floorSize->y);
    ofPopMatrix();
    // End applying material
    groundMaterial.end();
    // Switch on the light
    light.enable();
    // Draw fbx Character
    if(customFbx == false)
    {
        fbx1.draw();
    }
    else if (customFbx == true){
        fbx2.draw();
        //fbx2.drawSkeletons();
    }
    // Switch off the light
    light.disable();
    if(showSphere)
    {
        // Draw wireframed Sphere
        ofNoFill();
        ofSetColor(colorSphere, 140);
        ofDrawSphere(0, 0, 0, 120);
        ofFill();
    }
    // Draw light box
    //light.draw();
    // End light environment
    ofDisableLighting();
    ofDisableAlphaBlending();
    cam.end();
    ofDisableDepthTest();
    // If tweaking bones is activated, show name of bone + save changes to offset
    if(tglTweak)
    {
        ofSetColor(ofColor(255, 0, 0));
        zekton.drawString(jLbls[curJoint], width / 2 - jLbls[curJoint].size() * 20 / 2, height * 0.9);
        ofSetColor(255);
    }


    // If "a" is pressed, about text will appear
    if (bAbout == true || tglAbout) {
        ofFill();
        ofSetColor(50, 50, 50, 255);
        ofDrawRectangle(width / 2 - width * 0.3, height / 2 - height * 0.1, width * 0.6, height * 0.3);
        ofSetColor(0, 200, 200);
        stringstream ss;
        ss << "Mime Corporeal >> inspired by Etienne Decroux" << endl;
        ss << endl;
        ss << "Version v0.1" << endl;
        ss << endl;
        ss << "This example uses received rotation data from 'Chordata Mover'" << endl;
        ss << ">> Make sure 'Send Translation' is disabled in 'Chordata Mover' <<" << endl;
        ss << endl;
        ss << "Software developed by Jens Meisner (www.jensmeisner.net)" << endl;
        ss << endl;
        ss << "Licensed under GNU GENERAL PUBLIC LICENSE 3.0" << endl;
        ss << endl;
        ss << "contact@jensmeisner.net" << endl;
        ss << endl;
        ofDrawBitmapString(ss.str().c_str(), width / 2 - width * 0.3 + 40, height / 2 - height * 0.1 + 40);
        ofSetColor(255);
    }
}

//--------------------------------------------------------------
void ofApp::receiveOsc(){
    // Check for waiting messages
    while(receiver.hasWaitingMessages()){
        // Get the next message
        ofxOscMessage m;
        receiver.getNextMessage(m);
        //cout << m << endl;
        // If messages adresses bone, read incoming quaternion and apply it to joint rotation array
        if(m.getAddress() == "Hips")
        {
            //jRot[0].set(m.getArgAsFloat(0), m.getArgAsFloat(1), m.getArgAsFloat(2), m.getArgAsFloat(3));
            jRot[0].w() = m.getArgAsFloat(0);
            jRot[0].x() = m.getArgAsFloat(1);
            jRot[0].y() = m.getArgAsFloat(2);
            jRot[0].z() = m.getArgAsFloat(3);
        }
        else if(m.getAddress() == "Spine")
        {
            //jRot[1].set(m.getArgAsFloat(0), m.getArgAsFloat(1), m.getArgAsFloat(2), m.getArgAsFloat(3));
            jRot[1].w() = m.getArgAsFloat(0);
            jRot[1].x() = m.getArgAsFloat(1);
            jRot[1].y() = m.getArgAsFloat(2);
            jRot[1].z() = m.getArgAsFloat(3);
        }
        else if(m.getAddress() == "Chest")
        {
            //jRot[2].set(m.getArgAsFloat(0), m.getArgAsFloat(1), m.getArgAsFloat(2), m.getArgAsFloat(3));
            jRot[2].w() = m.getArgAsFloat(0);
            jRot[2].x() = m.getArgAsFloat(1);
            jRot[2].y() = m.getArgAsFloat(2);
            jRot[2].z() = m.getArgAsFloat(3);
        }
        else if(m.getAddress() == "Neck")
        {
            //jRot[3].set(m.getArgAsFloat(0), m.getArgAsFloat(1), m.getArgAsFloat(2), m.getArgAsFloat(3));
            jRot[3].w() = m.getArgAsFloat(0);
            jRot[3].x() = m.getArgAsFloat(1);
            jRot[3].y() = m.getArgAsFloat(2);
            jRot[3].z() = m.getArgAsFloat(3);
        }
        else if(m.getAddress() == "Head")
        {
            //jRot[4].set(m.getArgAsFloat(0), m.getArgAsFloat(1), m.getArgAsFloat(2), m.getArgAsFloat(3));
            jRot[4].w() = m.getArgAsFloat(0);
            jRot[4].x() = m.getArgAsFloat(1);
            jRot[4].y() = m.getArgAsFloat(2);
            jRot[4].z() = m.getArgAsFloat(3);
        }
        else if(m.getAddress() == "HeadEnd")
        {
            //jRot[5].set(m.getArgAsFloat(0), m.getArgAsFloat(1), m.getArgAsFloat(2), m.getArgAsFloat(3));
            jRot[5].w() = m.getArgAsFloat(0);
            jRot[5].x() = m.getArgAsFloat(1);
            jRot[5].y() = m.getArgAsFloat(2);
            jRot[5].z() = m.getArgAsFloat(3);
        }
        else if(m.getAddress() == "LeftUpLeg")
        {
            //jRot[6].set(m.getArgAsFloat(0), m.getArgAsFloat(1), m.getArgAsFloat(2), m.getArgAsFloat(3));
            jRot[6].w() = m.getArgAsFloat(0);
            jRot[6].x() = m.getArgAsFloat(1);
            jRot[6].y() = m.getArgAsFloat(2);
            jRot[6].z() = m.getArgAsFloat(3);
        }
        else if(m.getAddress() == "LeftLeg")
        {
            //jRot[7].set(m.getArgAsFloat(0), m.getArgAsFloat(1), m.getArgAsFloat(2), m.getArgAsFloat(3));
            jRot[7].w() = m.getArgAsFloat(0);
            jRot[7].x() = m.getArgAsFloat(1);
            jRot[7].y() = m.getArgAsFloat(2);
            jRot[7].z() = m.getArgAsFloat(3);
        }
        else if(m.getAddress() == "LeftFoot")
        {
            //jRot[8].set(m.getArgAsFloat(0), m.getArgAsFloat(1), m.getArgAsFloat(2), m.getArgAsFloat(3));
            jRot[8].w() = m.getArgAsFloat(0);
            jRot[8].x() = m.getArgAsFloat(1);
            jRot[8].y() = m.getArgAsFloat(2);
            jRot[8].z() = m.getArgAsFloat(3);
        }
        else if(m.getAddress() == "LeftFootEnd")
        {
            //jRot[9].set(m.getArgAsFloat(0), m.getArgAsFloat(1), m.getArgAsFloat(2), m.getArgAsFloat(3));
            jRot[9].w() = m.getArgAsFloat(0);
            jRot[9].x() = m.getArgAsFloat(1);
            jRot[9].y() = m.getArgAsFloat(2);
            jRot[9].z() = m.getArgAsFloat(3);
        }
        else if(m.getAddress() == "RightUpLeg")
        {
            //jRot[10].set(m.getArgAsFloat(0), m.getArgAsFloat(1), m.getArgAsFloat(2), m.getArgAsFloat(3));
            jRot[10].w() = m.getArgAsFloat(0);
            jRot[10].x() = m.getArgAsFloat(1);
            jRot[10].y() = m.getArgAsFloat(2);
            jRot[10].z() = m.getArgAsFloat(3);
        }
        else if(m.getAddress() == "RightLeg")
        {
            //jRot[11].set(m.getArgAsFloat(0), m.getArgAsFloat(1), m.getArgAsFloat(2), m.getArgAsFloat(3));
            jRot[11].w() = m.getArgAsFloat(0);
            jRot[11].x() = m.getArgAsFloat(1);
            jRot[11].y() = m.getArgAsFloat(2);
            jRot[11].z() = m.getArgAsFloat(3);
        }
        else if(m.getAddress() == "RightFoot")
        {
            //jRot[12].set(m.getArgAsFloat(0), m.getArgAsFloat(1), m.getArgAsFloat(2), m.getArgAsFloat(3));
            jRot[12].w() = m.getArgAsFloat(0);
            jRot[12].x() = m.getArgAsFloat(1);
            jRot[12].y() = m.getArgAsFloat(2);
            jRot[12].z() = m.getArgAsFloat(3);
        }
        else if(m.getAddress() == "RightFootEnd")
        {
            //jRot[13].set(m.getArgAsFloat(0), m.getArgAsFloat(1), m.getArgAsFloat(2), m.getArgAsFloat(3));
            jRot[13].w() = m.getArgAsFloat(0);
            jRot[13].x() = m.getArgAsFloat(1);
            jRot[13].y() = m.getArgAsFloat(2);
            jRot[13].z() = m.getArgAsFloat(3);
        }
        else if(m.getAddress() == "LeftShoulder")
        {
            //jRot[14].set(m.getArgAsFloat(0), m.getArgAsFloat(1), m.getArgAsFloat(2), m.getArgAsFloat(3));
            jRot[14].w() = m.getArgAsFloat(0);
            jRot[14].x() = m.getArgAsFloat(1);
            jRot[14].y() = m.getArgAsFloat(2);
            jRot[14].z() = m.getArgAsFloat(3);
        }
        else if(m.getAddress() == "LeftForeArm")
        {
            //jRot[15].set(m.getArgAsFloat(0), m.getArgAsFloat(1), m.getArgAsFloat(2), m.getArgAsFloat(3));
            jRot[15].w() = m.getArgAsFloat(0);
            jRot[15].x() = m.getArgAsFloat(1);
            jRot[15].y() = m.getArgAsFloat(2);
            jRot[15].z() = m.getArgAsFloat(3);
        }
        else if(m.getAddress() == "LeftArm")
        {
            //jRot[16].set(m.getArgAsFloat(0), m.getArgAsFloat(1), m.getArgAsFloat(2), m.getArgAsFloat(3));
            jRot[16].w() = m.getArgAsFloat(0);
            jRot[16].x() = m.getArgAsFloat(1);
            jRot[16].y() = m.getArgAsFloat(2);
            jRot[16].z() = m.getArgAsFloat(3);
        }
        else if(m.getAddress() == "LeftHand")
        {
            //jRot[17].set(m.getArgAsFloat(0), m.getArgAsFloat(1), m.getArgAsFloat(2), m.getArgAsFloat(3));
            jRot[17].w() = m.getArgAsFloat(0);
            jRot[17].x() = m.getArgAsFloat(1);
            jRot[17].y() = m.getArgAsFloat(2);
            jRot[17].z() = m.getArgAsFloat(3);
        }
        else if(m.getAddress() == "LeftHandEnd")
        {
            //jRot[18].set(m.getArgAsFloat(0), m.getArgAsFloat(1), m.getArgAsFloat(2), m.getArgAsFloat(3));
            jRot[18].w() = m.getArgAsFloat(0);
            jRot[18].x() = m.getArgAsFloat(1);
            jRot[18].y() = m.getArgAsFloat(2);
            jRot[18].z() = m.getArgAsFloat(3);
        }
        else if(m.getAddress() == "RightShoulder")
        {
            //jRot[19].set(m.getArgAsFloat(0), m.getArgAsFloat(1), m.getArgAsFloat(2), m.getArgAsFloat(3));
            jRot[19].w() = m.getArgAsFloat(0);
            jRot[19].x() = m.getArgAsFloat(1);
            jRot[19].y() = m.getArgAsFloat(2);
            jRot[19].z() = m.getArgAsFloat(3);
        }
        else if(m.getAddress() == "RightArm")
        {
            //jRot[20].set(m.getArgAsFloat(0), m.getArgAsFloat(1), m.getArgAsFloat(2), m.getArgAsFloat(3));
            jRot[20].w() = m.getArgAsFloat(0);
            jRot[20].x() = m.getArgAsFloat(1);
            jRot[20].y() = m.getArgAsFloat(2);
            jRot[20].z() = m.getArgAsFloat(3);
        }
        else if(m.getAddress() == "RightForeArm")
        {
            //jRot[21].set(m.getArgAsFloat(0), m.getArgAsFloat(1), m.getArgAsFloat(2), m.getArgAsFloat(3));
            jRot[21].w() = m.getArgAsFloat(0);
            jRot[21].x() = m.getArgAsFloat(1);
            jRot[21].y() = m.getArgAsFloat(2);
            jRot[21].z() = m.getArgAsFloat(3);
        }
        else if(m.getAddress() == "RightHand")
        {
            //jRot[22].set(m.getArgAsFloat(0), m.getArgAsFloat(1), m.getArgAsFloat(2), m.getArgAsFloat(3));
            jRot[22].w() = m.getArgAsFloat(0);
            jRot[22].x() = m.getArgAsFloat(1);
            jRot[22].y() = m.getArgAsFloat(2);
            jRot[22].z() = m.getArgAsFloat(3);
        }
        else if(m.getAddress() == "RightHandEnd")
        {
            //jRot[23].set(m.getArgAsFloat(0), m.getArgAsFloat(1), m.getArgAsFloat(2), m.getArgAsFloat(3));
            jRot[23].w() = m.getArgAsFloat(0);
            jRot[23].x() = m.getArgAsFloat(1);
            jRot[23].y() = m.getArgAsFloat(2);
            jRot[23].z() = m.getArgAsFloat(3);
        }
        else if(m.getAddress() == "HipsT")
        {
            hipsT.set(m.getArgAsFloat(0) / 10, m.getArgAsFloat(1) / 10, m.getArgAsFloat(2) / 10);
        }
    }
}


//--------------------------------------------------------------
void ofApp::offsetGyros(){
    cout<<"Offset"<<endl;
    for(int i = 0; i < jointCount; i++)
    {
        //Calculate the difference between the last offset differences and reset
        //zeroDiff[i] = jRot[i] * zeroOffset[i].inverse();
        zeroDiff[i] = jRot[i] * zeroOffset[i].inverse();
        zeroDiff[i].normalize();
        //cout<<zeroDiff[i]<<endl;
    }
}

//--------------------------------------------------------------
void ofApp::setOrientationQuat(ofVec3f euler){

    glm::quat QuatAroundX = glm::angleAxis(ofDegToRad(euler.x), glm::vec3(1.0, 0.0, 0.0));
    glm::quat QuatAroundY = glm::angleAxis(ofDegToRad(euler.y), glm::vec3(0.0, 1.0, 0.0));
    glm::quat QuatAroundZ = glm::angleAxis(ofDegToRad(euler.z), glm::vec3(0.0, 0.0, 1.0));
    offsetDiff = QuatAroundX * QuatAroundY * QuatAroundZ;
}

//--------------------------------------------------------------
void ofApp::portInChanged(string& port){
    // Reinitiate osc receiver
    portIn = ofToInt(port);
    receiver.setup(portIn);
    ofLog() << "listening for osc messages on port " << portIn;

}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
    if(key == 'f' || key == 'F')
    {
        bFull = !bFull;
    }
    if(key == 'm' || key == 'M')
    {
        bGui = !bGui;
        panel->setHidden(bGui);
    }
    if(key == 'a' || key == 'A')
    {
        bAbout = !bAbout;
    }
    if(key == 'z' || key == 'Z')
    {
        offsetGyros();
    }
    if (key == OF_KEY_UP)
    {
        // Scroll through reference points upwards
        if(tglTweak)
        {
            curJoint = curJoint + 1;
            if (curJoint > int(jLbls.size() - 1))
            {
                curJoint = 0;
            }
            boneTweak.set(ofVec3f(0, 0, 0));
        }
    }
    if (key == OF_KEY_DOWN)
    {
        // Scroll through reference points downwards
        if(tglTweak)
        {
            curJoint = curJoint - 1;
            if (curJoint < 0)
            {
                curJoint = int(jLbls.size()) - 1;
            }
            boneTweak.set(ofVec3f(0, 0, 0));
        }
    }
}
